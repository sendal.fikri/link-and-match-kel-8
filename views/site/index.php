<div class="container">
    <div class="index-1">
        <section class="content" style="padding-left:0; padding-right:0">
            <div class="bg-aqua jumbotron">
                <h2 style="font-size:42px;">
                    <i class=" fa fa-graduation-cap""></i>
              Selamat datang, Usamah
              </h2>
              <h3>Pilihlah menu-menu dibawah untuk memulai aktivitas akademik.</h3>
            </div>
            <div class=" row">
                        <div class="col-md-2">
                            <div class="thumbnail" style="text-align:center; padding-top:10px; color: #3c8dbc;">
                                <i class="fa fa-clock-o fa-4x"></i>
                                <div class="caption" style="padding:8px 0 0;">
                                    <h4 style="font-size:16px;">Daftar Kehadiran</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="thumbnail" style="text-align:center; padding-top:10px; color: #3c8dbc;">
                                <i class="fa fa-money fa-4x"></i>
                                <div class="caption" style="padding:8px 0 0;">
                                    <h4 style="font-size:16px;">Tagihan Biaya Tugas</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="thumbnail" style="text-align:center; padding-top:10px; color: #3c8dbc;">
                                <i class="fa fa-graduation-cap fa-4x"></i>
                                <div class="caption" style="padding:8px 0 0;">
                                    <h4 style="font-size:16px;">Kartu Rencana Studi</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="thumbnail" style="text-align:center; padding-top:10px; color: #3c8dbc;">
                                <i class="fa  fa-clone fa-4x"></i>
                                <div class="caption" style="padding:8px 0 0;">
                                    <h4 style="font-size:16px;">Kartu Hasil Studi</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="thumbnail" style="text-align:center; padding-top:10px; color: #3c8dbc;">
                                <i class="fa fa-users fa-4x"></i>
                                <div class="caption" style="padding:8px 0 0;">
                                    <h4 style="font-size:16px;">Bimbingan Akademik</h4>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <a href="lkta/index">
                                <div class="thumbnail" style="text-align:center; padding-top:10px; color: #3c8dbc;">
                                    <i class="fa fa-book fa-4x"></i>
                                    <div class=" caption" style="padding:8px 0 0;">
                                        <h4 style="font-size:16px;">Tugas Akhir</h4>
                                    </div>
                                </div>
                            </a>
                        </div>
            </div>
            <!-- /.box -->
        </section>
    </div>
</div>